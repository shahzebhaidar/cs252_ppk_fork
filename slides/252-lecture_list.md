1. Basic Protocols, IP addressing and CIDR subnetting.
2. ARP and DNS
3. DHCP, iptables
4. netcat, email - smtp, imap and webmail
5. SSH (Piyush)
6. HTTP, REST and curl
7. Web Security (Piyush)
8. Web stack - MEAN
9. Web Performance Tuning - Caching, CDN (Piyush, no slides)
10. Load balancing using haproxy (Saurabh)
11. HTTPS and incorporating into the webapp
12. Deployment: containers and Docker